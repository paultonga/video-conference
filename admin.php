<?php include "connect.php";?>
<?php include "functions.php";?>

<html>
<head>
<title>Admin Panel | Conference System</title>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/jquery-ui.min.js" type="text/javascript"></script>

    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

</head>
<body>
<?php include "title_bar.php";?>
<div class="container">
<?php if($type!=1){header('location: profile.php');} ?>
    <div class="jumbotron">
<h3>Adminstrator Panel - Main page</h3>
        <div class="btn-group">
            <a href="adminuserinformation.php" class="btn btn-default" role="button">Users Informations</a>
            <a href="conferenceinformation.php" class="btn btn-default" role="button">Manage Conferences</a>
        </div>
    </div>

    </div>
</body>
</html>