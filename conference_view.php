<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 5/17/14
 * Time: 3:39 PM
 */

include "connect.php";
include "functions.php";

?>
<html>
<head>
<title>View All Conferences | Conference System</title>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/jquery-ui.min.js" type="text/javascript"></script>

    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

</head>

<body>
<?php include "title_bar.php"; ?>
<div class="container">
    <div class="jumbotron">
        <h3>View all Conferences</h3>
        <p>Below is a list of all upcoming or happening conferences. You need to be logged in to be able to
        participate. Click the <a>Login</a> link above to login. If you don't have an account, just <a>Register</a></p>

    </div>

    <div class="col-md-11">
        <?php
        $listConference = mysql_query("SELECT * FROM conference ORDER By conf_date ASC LIMIT 15");

        while($row = mysql_fetch_array($listConference, MYSQL_ASSOC))
        {

            echo "<h4>";
            echo $row['title'];
            echo "</h4>";
            echo "<p>";
            echo $row['description'];
            echo "</p>";
            echo "<small>";
            echo "Starts on: ".$row['conf_date']." by ".$row['start_time'];
            echo "</small>";
            if(!loggedin())
                echo "<br><a role='button' class='btn btn-default'  href='login.php'>Login to Join</a>";
            else
                echo "<br><a role='button' class='btn btn-default'  href='c.php?ID=".$row['id']."'>Join Conference</a>";
            echo "<hr>";
        }
        mysql_free_result($listConference);

        ?>

    </div>


</div>
</body>
</html>