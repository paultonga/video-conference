<?php
include "connect.php";
include "functions.php";
?>

<html>
<head>
<title>Create Conference - Admin Panel</title>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/jquery-ui.min.js" type="text/javascript"></script>

    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
</head>
<body>
<?php
include "title_bar.php";
?>
<?php //Rejecting a user to the profile page if he is not an admin 
if($type!=1){header ('location: index.php');}
?>
<div class="container">
    <div class="jumbotron">
        <h3>Create Conference</h3>
        <p>Dear Admin, you need to carefully enter below information in order to create a conference</p>
        <a href="admin.php" class="btn btn-default" role="button">Admin Panel</a>
        <a href="conferenceinformation.php" class="btn btn-default" role="button">Manage Conferences</a>
    </div>


    <div class="col-md-6">
<form method="post" role="form" >
<?php
 if(isset($_POST['submit'])){
	 $title = $_POST['title'];
	 $description = $_POST['description'];
	 $year =(int)$_POST['year'];
	 $month =(int)$_POST['month'];
	 $day =(int)$_POST['day'];
	 $start_hour =(int)$_POST['hour1'];
	 $start_minute =(int)$_POST['minute1'];
	 $end_hour =(int)$_POST['hour2'];
	 $end_minute =(int)$_POST['minute2'];
	 $date = date("$year-$month-$day");
	 $start_time = date("$start_hour:$start_minute:00");
	 $end_time = date("$end_hour:$end_minute:00");
	 $room = $_POST['room'];
	 $st = strtotime($start_time);
	 $et = strtotime($end_time);
	 //getting the presenter id from the database
	 $presenter = $_POST['presenter'];
	 $sql1 = mysql_query("SELECT id FROM users WHERE username = '$presenter'");
	 $row1 = mysql_num_rows($sql1);
	 if($row1 != 1){echo "$presenter could not be found on the system, make sure the username was correctly spelled!!";}
	 else{
	 $result1 = mysql_fetch_array($sql1);
	 $p1 = $result1['id'];
	 }
	 //getting the user1 id from the database
	 $user1 = $_POST['user1'];
	 $sql2 = mysql_query("SELECT id FROM users WHERE username = '$user1'");
	 $row2 = mysql_num_rows($sql2);
	 if($row2 != 1){echo "$user1 could not be found on the system, make sure the username was correctly selected!!";}else{
	 $result2 = mysql_fetch_array($sql2);
	 $u1 = $result2['id'];
	 }
	 //getting the user2 id from the database
	 $user2 = $_POST['user2'];
	 $sql3 = mysql_query("SELECT id FROM users WHERE username = '$user2'");
	 $row3 = mysql_num_rows($sql3);
	 if($row3 != 1){echo "$user2 could not be found on the system, make sure the username was correctly selected!!";}else{
	 $result3 = mysql_fetch_array($sql3);
	 $u2 = $result3['id'];
	 }
	 //Presenter and users 1 and 2 from the database
	 $pres = mysql_query("SELECT presenter, conf_date, end_time FROM conference WHERE presenter = '$p1' AND conf_date = '$date'");
	 $num = mysql_num_rows($pres);
	 $fetch = mysql_fetch_array($pres);
	 $pres1 = mysql_query("SELECT presenter, user1, user2 FROM conference WHERE presenter = '$u1' OR presenter = '$u2' ");
	 $num1 = mysql_num_rows($pres1);
if(empty($title) or empty($description) or empty($date) or empty($start_time) or empty($end_time) or empty($room) or empty($presenter) or empty($user1) or empty($user2))
	 { 
	 echo "Fields empty, unable to create the conference.<br> You need to check that all fields are correctly filled";
	 }
	 //Checking if the first time is greater than the second
	 else if($st>$et){echo "Check your times, the finishing time should be greater than the starting one";}
	 else if($start_time==$end_time){echo"A conference cannot start and finish at the same time";}
	 else if($u1==$u2){echo"The same user cannot attend the the conference at differente locations at the the same time";}
	 else if($num1>0){echo"Presenter cannot be user at the same time";}
	 else if($num>0 && $st-strtotime($fetch['end_time'])<=3600){echo"A presenter should have a next conference at least one hour after the end of a previous one";}
	 else{$conf_query = mysql_query("INSERT INTO conference VALUES('','$title','$description','$date','$start_time','$end_time','$room','','$p1','$u1','$u2')");
	 echo "Conference successfuly created";}
	 
 }
?>
<div class="form-group">
	<label>Conference Title</label>
    <input class="form-control" type="text" name="title" value="<?php echo isset($_POST['title'])? $_POST['title'] : "" ?>" />
    </div>
<div class="form-group">
    <label>Conference Date</label>

    <select name="year" class="form-control">
    	<option value="<?php echo isset($_POST['year'])? $_POST['year'] : "" ?>">Year</option>
    	<option value="2014">2014</option>
        <option value="2015">2015</option>
        <option value="2016">2016</option>
        <option value="2017">2017</option>
        <option value="2018">2018</option>
        <option value="2019">2019</option>
        <option value="2020">2020</option>
    </select>


    <select name="month" class="form-control" >
    	<option value="<?php echo isset($_POST['month'])? $_POST['month'] : "" ?>">Month</option>
    	<option value="01">January</option>
        <option value="02">February</option>
        <option value="03">March</option>
        <option value="04">April</option>
        <option value="05">May</option>
        <option value="06">June</option>
        <option value="07">July</option>
        <option value="08">August</option>
        <option value="09">September</option>
        <option value="10">October</option>
        <option value="11">November</option>
        <option value="12">December</option>
    </select>


    <select name="day" class="form-control">
    	<option value="<?php echo isset($_POST['day'])? $_POST['day'] : "" ?>">Day</option>
    	<option value="01">1</option>
        <option value="02">2</option>
        <option value="03">3</option>
        <option value="04">4</option>
        <option value="05">5</option>
        <option value="06">6</option>
        <option value="07">7</option>
        <option value="08">8</option>
        <option value="09">9</option>
        <option value="10">10</option>
        <option value="11">11</option>
        <option value="12">12</option>
        <option value="13">13</option>
        <option value="14">14</option>
        <option value="15">15</option>
        <option value="16">16</option>
        <option value="17">17</option>
        <option value="18">18</option>
        <option value="19">19</option>
        <option value="20">20</option>
        <option value="21">21</option>
        <option value="22">22</option>
        <option value="23">23</option>
        <option value="24">24</option>
        <option value="25">25</option>
        <option value="26">26</option>
        <option value="27">27</option>
        <option value="28">28</option>
        <option value="29">29</option>
        <option value="30">30</option>
        <option value="31">31</option>
    </select>

    </div>
    <div class="form-group">
    <label>Starting Time</label>
    <select name="hour1" class="form-control">
    	<option value="<?php echo isset($_POST['hour1'])? $_POST['hour1'] : "" ?>">Hour</option>
    	<option value="01">1</option>
        <option value="02">2</option>
        <option value="03">3</option>
        <option value="04">4</option>
        <option value="05">5</option>
        <option value="06">6</option>
        <option value="07">7</option>
        <option value="08">8</option>
        <option value="09">9</option>
        <option value="10">10</option>
        <option value="11">11</option>
        <option value="12">12</option>
        <option value="13">13</option>
        <option value="14">14</option>
        <option value="15">15</option>
        <option value="16">16</option>
        <option value="17">17</option>
        <option value="18">18</option>
        <option value="19">19</option>
        <option value="20">20</option>
        <option value="21">21</option>
        <option value="22">22</option>
        <option value="23">23</option>
        <option value="24">24</option>
    </select>
    <select name="minute1" class="form-control">
    	<option value="<?php echo isset($_POST['minute1'])? $_POST['minute1'] : "" ?>">Minute</option>
    	<option value="00">00</option>
    	<option value="01">1</option>
        <option value="02">2</option>
        <option value="03">3</option>
        <option value="04">4</option>
        <option value="05">5</option>
        <option value="06">6</option>
        <option value="07">7</option>
        <option value="08">8</option>
        <option value="09">9</option>
        <option value="10">10</option>
        <option value="11">11</option>
        <option value="12">12</option>
        <option value="13">13</option>
        <option value="14">14</option>
        <option value="15">15</option>
        <option value="16">16</option>
        <option value="17">17</option>
        <option value="18">18</option>
        <option value="19">19</option>
        <option value="20">20</option>
        <option value="21">21</option>
        <option value="22">22</option>
        <option value="23">23</option>
        <option value="24">24</option>
        <option value="25">25</option>
        <option value="26">26</option>
        <option value="27">27</option>
        <option value="28">28</option>
        <option value="29">29</option>
        <option value="30">30</option>
        <option value="31">31</option>
        <option value="32">32</option>
        <option value="33">33</option>
        <option value="34">34</option>
        <option value="35">35</option>
        <option value="36">36</option>
        <option value="37">37</option>
        <option value="38">38</option>
        <option value="39">39</option>
        <option value="40">40</option>
        <option value="41">41</option>
        <option value="42">42</option>
        <option value="43">43</option>
        <option value="44">44</option>
        <option value="45">45</option>
        <option value="46">46</option>
        <option value="47">47</option>
        <option value="48">48</option>
        <option value="49">49</option>
		<option value="50">50</option>
        <option value="51">51</option>
        <option value="52">52</option>
        <option value="53">53</option>
        <option value="54">54</option>
        <option value="55">55</option>
        <option value="56">56</option>
        <option value="57">57</option>
        <option value="58">58</option>
        <option value="59">59</option>
    </select>
    </div>
    <div class="form-group">
    <label>End time</label>
    <select name="hour2" class="form-control">
    	<option value="<?php echo isset($_POST['hour2'])? $_POST['hour2'] : "" ?>">Hour</option>
    	<option value="01">1</option>
        <option value="02">2</option>
        <option value="03">3</option>
        <option value="04">4</option>
        <option value="05">5</option>
        <option value="06">6</option>
        <option value="07">7</option>
        <option value="08">8</option>
        <option value="09">9</option>
        <option value="10">10</option>
        <option value="11">11</option>
        <option value="12">12</option>
        <option value="13">13</option>
        <option value="14">14</option>
        <option value="15">15</option>
        <option value="16">16</option>
        <option value="17">17</option>
        <option value="18">18</option>
        <option value="19">19</option>
        <option value="20">20</option>
        <option value="21">21</option>
        <option value="22">22</option>
        <option value="23">23</option>
    </select>
    <select name="minute2" class="form-control">
    	<option value="<?php echo isset($_POST['minute2'])? $_POST['minute2'] : "" ?>">Minute</option>
    	<option value="00">00</option>
    	<option value="01">1</option>
        <option value="02">2</option>
        <option value="03">3</option>
        <option value="04">4</option>
        <option value="05">5</option>
        <option value="06">6</option>
        <option value="07">7</option>
        <option value="08">8</option>
        <option value="09">9</option>
        <option value="10">10</option>
        <option value="11">11</option>
        <option value="12">12</option>
        <option value="13">13</option>
        <option value="14">14</option>
        <option value="15">15</option>
        <option value="16">16</option>
        <option value="17">17</option>
        <option value="18">18</option>
        <option value="19">19</option>
        <option value="20">20</option>
        <option value="21">21</option>
        <option value="22">22</option>
        <option value="23">23</option>
        <option value="24">24</option>
        <option value="25">25</option>
        <option value="26">26</option>
        <option value="27">27</option>
        <option value="28">28</option>
        <option value="29">29</option>
        <option value="30">30</option>
        <option value="31">31</option>
        <option value="32">32</option>
        <option value="33">33</option>
        <option value="34">34</option>
        <option value="35">35</option>
        <option value="36">36</option>
        <option value="37">37</option>
        <option value="38">38</option>
        <option value="39">39</option>
        <option value="40">40</option>
        <option value="41">41</option>
        <option value="42">42</option>
        <option value="43">43</option>
        <option value="44">44</option>
        <option value="45">45</option>
        <option value="46">46</option>
        <option value="47">47</option>
        <option value="48">48</option>
        <option value="49">49</option>
		<option value="50">50</option>
        <option value="51">51</option>
        <option value="52">52</option>
        <option value="53">53</option>
        <option value="54">54</option>
        <option value="55">55</option>
        <option value="56">56</option>
        <option value="57">57</option>
        <option value="58">58</option>
        <option value="59">59</option>
    </select>
        </div>
    <div class="form-group">
    <label>Room</label>
    <input class="form-control" type="text" name="room" value="<?php echo isset($_POST['room'])? $_POST['room'] : "" ?>" />
    </div>
    <div class="form-group">
    <label>Select a presenter</label>
    <select name="presenter" class="form-control">
    <?php
	$query = mysql_query("SELECT * FROM users ORDER BY 'username' ASC");
	echo"<option>Presenter</option>";
	while($row = mysql_fetch_array($query)){
		if($row['type']!=1)
		echo "<option>".$row['username']."</option>";
	}
	?>
    </select>
        </div>
<div class="form-group">
    
     <label>Select user 1</label>
    <select name="user1" class="form-control">
    <?php
	echo"<option>User1</option>";
	$query1 = mysql_query("SELECT * FROM users ORDER BY 'username' ASC");
	while($row = mysql_fetch_array($query1)){
		if($row['type']!=1)
		echo "<option>".$row['username']."</option>";
	}
	?>
    </select>
</div>
<div class="form-group">
    <label>Select user 2</label>
    <select name="user2" class="form-control">
    <?php
	echo"<option>User2</option>";
	$query2 = mysql_query("SELECT * FROM users ORDER BY 'username' ASC");
	while($row = mysql_fetch_array($query2)){
		if($row['type']!=1)
		echo "<option>".$row['username']."</option>";
	}
	?>
    </select>
    </div>
<div class="form-group">
     <label>Description</label>
    <textarea class="form-control" type="text" name="description" value="<?php echo isset($_POST['description'])? $_POST['description'] : "" ?>"></textarea>
    <br /><br />
   </div>
<div class="form-group">
    <input class="btn btn-success btn-lg" type="submit" name="submit" value="Submit" />
    <input class="btn btn-danger btn-lg" type="reset" value="Clear" />
</div>
</form>
    </div>
</div>
</body>
</html>