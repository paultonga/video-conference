<?php
include "connect.php";
include "functions.php";
?>
<html>
<head>
<title>Edit Conference</title>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/jquery-ui.min.js" type="text/javascript"></script>

    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
</head>

<body>
<?php include "title_bar.php"; ?>
<div class="container">
    <div class="jumbotron">
        <h3>Edit Conference</h3>
        <p>You need to manually and carefully change these information</p>
        <a href="admin.php" class="btn btn-default" role="button">Admin Panel</a>
        <a href="conferenceinformation.php" class="btn btn-default" role="button">Manage Conferences</a>
    </div>

<div class="col-md-6">
  <?php
$ID =$_GET["ID"];
$sql = mysql_query("SELECT * FROM conference WHERE id='$ID' ");
    while($reed = mysql_fetch_array($sql))
	{
	    $ID = $reed["id"];
		$title = $reed["title"];
		$cdate = $reed["conf_date"];
		$conf_date = date($cdate);//conference date in date format to be inserted into the database, conference table
		$stime = $reed["start_time"];
		$start_time = date($stime);//conference starting time in date format, to be inserted into the database, conference table 
		$etime = $reed["end_time"];
		$end_time = date($etime);//conference ending time in date format, to be inserted into conference table of the database
		$room_no = $reed["room_no"];
		$description = $reed["description"];
		$presenter = $reed["presenter"];
		$user1 = $reed["user1"];
		$user2 = $reed["user2"];
		// getting the presenter's username from the users table from the database
		$q1 = mysql_query("SELECT username FROM users WHERE id = '$presenter'");
		$fetch1 = mysql_fetch_array($q1);
		$pres = $fetch1['username'];
		// getting user1's username from the database
		$q2 = mysql_query("SELECT username FROM users WHERE id = '$user1'");
		$fetch2 = mysql_fetch_array($q2);
		$us1 = $fetch2['username'];
		// getting user2's username from the database
		$q3 = mysql_query("SELECT username FROM users WHERE id = '$user2'");
		$fetch3 = mysql_fetch_array($q3);
		$us2 = $fetch3['username'];
  echo '<form name="form1" method="post" action="editconference2.php?ID='.$ID.'">


<div class="form-group">
    <label for="txttitle">Title</label>
      <input class="form-control" type="text" name="txttitle" value="'.$title.'">
  </div>

  <div class="form-group">

    <label for="txtconfdate">Conference Date</label>
      <input class="form-control" type="text" name="txtconfdate" value="'.$conf_date.'" >
  </div>
  

  <div class="form-group">

    <label for="txtstarttime">Start-Time</label>
      <input class="form-control" type="text" name="txtstarttime"  value="'.$start_time.'">
  </div>

  <div class="form-group">

    <label for="txtendtime">End-Time</label>
      <input class="form-control" type="text" name="txtendtime"  value="'.$end_time.'">

  </div>
  <div class="form-group">

    <label for="txtroomno">Room-No</label>
      <input class="form-control" type="text" name="txtroomno"  value="'.$room_no.'">
  </div>
  
  <div class="form-group">
    <label for="txtdescription">Description</label>
      <input class="form-control" type="text" name="txtdescription"  value="'.$description.'" >
  </div>
  
  <div class="form-group">

    <label for="txtpresenter">Presenter</label>
      <input class="form-control" type="text" name="txtpresenter"  value="'.$pres.'">
  </div>
  
   <div class="form-group">

    <label for="txtuser1">User1</label>
      <input class="form-control" type="text" name="txtuser1"  value="'.$us1.'">
  </div>
  
  <div class="form-group">

    <label for="txtuser2">User2</label>
      <input class="form-control" type="text" name="txtuser2"  value="'.$us2.'">
  </div>

  <div class="form-group">

     <input class="btn btn-success btn-lg" type="submit" name="edit" id="edit" value="Edit">
 </div>
</form>';
}		 
?>
    </div>
</div>
</body>
</html>