<?php include "connect.php";?>
<?php include "functions.php";?>
<html>
<head>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/jquery-ui.min.js" type="text/javascript"></script>

    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <title>Error Page</title>
</head>
<body>
<?php include "title_bar.php";?>
<div class="container">
    <div class="jumbotron">
        <h3>Error!</h3>
        <p> You have not been assigned to this conference. Please contact the administrator</p>
        <a href="index.php" class="btn btn-default" role="button">Go Home</a>
        <a href="conference_view.php" class="btn btn-default" role="button">View Conferences</a>
    </div>



</div>

</body>
</html>