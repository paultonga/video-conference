<?php include "connect.php"; ?>
<?php include "functions.php"; ?>
<html>
<head>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/jquery-ui.min.js" type="text/javascript"></script>
    <script src="js/moment.min.js" type="text/javascript"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <style>
        span.glyphicon-film {
            display: :inline;
            top: 11px;
        }
    </style>
<title>Home | Conference System</title>
</head>
<body>
<?php include "title_bar.php"; ?>
<div class="container">
    <div class="jumbotron">
        <img class="img-thumbnail" src="conf.jpg" width="368" height="194">
        <div class="col-md-7 pull-right">
        <h4>Welcome to Our Conference System</h4>
        <p>Our conference system allows users to participate in and present conferences in any field of study. Click the <a href="login.php">Login page</a> to login in to
        your account. If you do not have an account, you can create one using the <a href="register.php">Register Page</a>.</p>
        </div>
    </div>



    <div class="col-md-6">
        <h4>Upcoming Conferences</h4>
        <?php
            $listConference = mysql_query("SELECT title, description, conf_date FROM conference ORDER By conf_date ASC LIMIT 3");

            while($row = mysql_fetch_array($listConference, MYSQL_ASSOC))
            {

                echo "<h4><a href='conference_view.php'>";
                echo $row['title'];
                echo "</a></h4>";
                echo "<p>";
                echo $row['description'];
                echo "</p>";

                echo "<hr>";

            }
        mysql_free_result($listConference);

        ?>
    </div>
    <div class="col-md-6">

        <h4>Features of Our System</h4>
        <h4>  Multi-directional Video/Audio</h4>
        <img src="img/conf-icon.png" width="32" height="32" class="img-circle pull-left">
        <div class="col-sm-11">
            <p>Our system allows users from different conference rooms to see and hear each other concurrently
            </p><hr>
        </div>

        <h4>Chatting System</h4>
        <img src="img/chat-icon.png" width="32" height="32" class="img-circle pull-left">
        <div class="col-sm-11">
        <p>Our system offers users online chat facility. Users can chat privately or publicly in a chat room</p><hr>
        </div>

        <h4>File Sharing</h4>
           <img src="img/file-icon.png" height="32" width="32" class="img-circle pull-left">
        <div class="col-sm-11">
        <p>Users can share files with each other during conference sessions, using our file sharing capabilites</p>
        </div>
    </div>


</div>
</body>
</html>