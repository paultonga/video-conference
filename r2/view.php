<?php
include "../connect.php";
include "../functions.php";
session_start();
date_default_timezone_set("Asia/Istanbul");
$confN = $_SESSION['confID'];

$x = mysql_query("SELECT * from conference where id = '$confN'");
$y = mysql_fetch_array($x, MYSQL_BOTH);

$confTitle = $y['title'];
$cDate =  $y['conf_date'];
$sTime =  $y['start_time'];
$eTime =  $y['end_time'];
$t = new DateTime('now');
$today = $t->format('d/m/Y');
$strDate = date('d/m/Y', strtotime($cDate));
$strNow = (int) $t->format('Gis');
$strStart = (int) date('Gis', strtotime($sTime));
$strEnd = (int) date('Gis', strtotime($eTime));

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
    <title>Room Two Page | Conference System</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script type="text/javascript" src="swfobject.js"></script>
    <script type="text/javascript">
        <!-- Adobe recommends that developers use SWFObject2 for Flash Player detection. -->
            <!-- For more information see the SWFObject page at Google code (http://code.google.com/p/swfobject/). -->
        <!-- Information is also available on the Adobe Developer Connection Under "Detecting Flash Player versions and embedding SWF files with SWFObject 2" -->
        <!-- Set to minimum required Flash Player version or 0 for no version detection -->
        var swfVersionStr = "9.0.124";
        <!-- xiSwfUrlStr can be used to define an express installer SWF. -->
        var xiSwfUrlStr = "";
        var flashvars = {};
        flashvars.name = escape('<?php echo $confN; ?>');
        var params = {};
        params.quality = "best";
        params.bgcolor = "#ffffff";
        params.play = "true";
        params.loop = "true";
        params.wmode = "window";
        params.scale = "noborder";
        params.menu = "true";
        params.devicefont = "false";
        params.salign = "";
        params.allowscriptaccess = "sameDomain";
        var attributes = {};
        attributes.id = "userTwo";
        attributes.name = "userTwo";
        attributes.align = "middle";
        swfobject.createCSS("html", "height:100%; background-color: #ffffff;");
        swfobject.createCSS("body", "margin:0; padding:0; overflow:hidden; height:100%;");
        swfobject.embedSWF(
            "userTwo.swf", "flashContent",
            "600", "550",
            swfVersionStr, xiSwfUrlStr,
            flashvars, params, attributes);
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/jquery-ui.min.js" type="text/javascript"></script>

    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
</head>
<body>
<?php
include "../title_bar.php";
if(isset($_SESSION['user2']) && $_SESSION['user2'] != null)
{
    if(strcmp($today,$strDate)==0 && $strNow >= $strStart && $strEnd >= $strNow )
    {
?>
<div class="container">
    <br/><br/><br/>
    <div class="col-md-7">

        <h4>Room Two Page | <?php print $confTitle; ?></h4>

        <hr>
        <!-- SWFObject's dynamic embed method replaces this alternative HTML content for Flash content when enough JavaScript and Flash plug-in support is available. -->
        <div id="flashContent">
            <a href="http://www.adobe.com/go/getflash">
                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
            </a>
            <p>This page requires Flash Player version 9.0.124 or higher.</p>
        </div>
    </div>

    <div class="col-md-5">
        <h4>Chat</h4>
        <hr>

        <?php include_once ("index.php"); ?>


        <hr>
        <a role="button" class="btn btn-default btn-success" href="javascript:void(0);"
           NAME="My Window Name" title=" Whiteboard "
           onClick=window.open("../whiteboard/show.php","Ratting","width=550,height=300,0,status=0,");>Open Whiteboard</a>
    </div>
    <?php


    }
    else
    { ?>
        <div class="container">
            <div class="jumbotron">
                <h3>Sorry!</h3>
                <p>This conference is scheduled to run from <?php print date('g:i A', strtotime($sTime)); ?> to
                    <?php print date('g:i A', strtotime($eTime)); ?> of
                    <?php print date('l jS F Y', strtotime($cDate)); ?>.
                    This page will only be accessible during that period.</p>
                <a href="../index.php" class="btn btn-sm btn-default" role="button">Go Home</a>
                <a href="../conference_view.php" class="btn btn-sm btn-default" role="button">View Conferences</a>
            </div>

        </div>
    <?php

    }
    }
    else
    {
        ?>
        <div class="container">
            <div class="jumbotron">
                <h3>Ooops!</h3>
                <p>You are not authorized to view this page. Contact the administrator</p>
                <a href="../index.php" class="btn btn-sm btn-default" role="button">Go Home</a>
                <a href="../conference_view.php" class="btn btn-sm btn-default" role="button">View Conferences</a>
            </div>

        </div>
    <?php
    }
    ?>

</body>
</html>
