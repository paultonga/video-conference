
<?php
if(loggedin()){
    ?>
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../index.php">Conference System</a>
            </div>

            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><a href="../index.php">Home</a></li>
                    <li><a href="../conference_view.php">Conferences</a></li>


                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href='../logout.php'>Log Out</a></li>
                    <li><a href='../profile.php'>Profile</a></li>

                </ul>
            </div><!--/.nav-collapse -->
    </div>
    </div>

<?php
   } else{
         ?>
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../index.php">Conference System</a>
            </div>

            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><a href="../index.php">Home</a></li>
                    <li><a href="../conference_view.php">Conferences</a></li>


                </ul>
                <ul class="nav navbar-nav navbar-right">

                    <li class="active"><a href='../login.php'>Log In</a></li>
                    <li><a href='../forgot_password.php'>Forgot Password</a></li>
                    <li><a href='../register.php'>Register</a></li>
                </ul>
            </div>
        </div>
    </div>

<?php
}
?>
