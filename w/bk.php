<?php require_once('MyConnection.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
    function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
    {
        if (PHP_VERSION < 6) {
            $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
        }

        $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

        switch ($theType) {
            case "text":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "long":
            case "int":
                $theValue = ($theValue != "") ? intval($theValue) : "NULL";
                break;
            case "double":
                $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
                break;
            case "date":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "defined":
                $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
                break;
        }
        return $theValue;
    }
}

mysql_select_db($database_MyConnection, $MyConnection);
$query_online_offline = "SELECT * FROM `online`";
$online_offline = mysql_query($query_online_offline, $MyConnection) or die(mysql_error());
$row_online_offline = mysql_fetch_assoc($online_offline);
$totalRows_online_offline = mysql_num_rows($online_offline);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>AJAX Whiteboard</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link href="whiteboard.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="whiteboard.js"></script>
</head>
<body>

<div id="whiteboard" onmousemove="javascript:handleMouseMove(event);" onmouseout="javascript:handleMouseOut(event);">
</div>
<div style="clear:both;"></div>
<div>
    <input type="button" onclick="clearWhiteboard();" value="Clear!" /> &nbsp;&nbsp;&nbsp; <a href="online.php?id=1&online=<?php if($row_online_offline['online'] == '1') { echo '0';} else { echo '1';} ?>"><?php if($row_online_offline['online'] == '1') { echo 'Close';} else { echo 'open';} ?></a>
</div>
</body>
</html>
<?php
mysql_free_result($online_offline);
?>
