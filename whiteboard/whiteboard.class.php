<?php
require_once('../whiteboard/config.php');
class Whiteboard {
    private $mMysqli;
    private $mMaxLoad = 100000;
    function __construct() {
    $this->mMysqli = new mysqli('localhost','root','conference1991','cmsystem');
    }
    function __destruct() {
    $this->mMysqli->close();
    }
    public function checkLoad() {
    $check_load = 'SELECT SUM(length) total_length FROM whiteboard';
    $result = $this->mMysqli->query($check_load);
    $row = $result->fetch_array(MYSQLI_ASSOC);
    if($row['total_length'] > $this->mMaxLoad) {
    $this->clearWhiteboard();
    return true;
    } else
    return false;
    }
    public function insertLines($lines, $session_id) {
        if($lines) {
        $array_lines = explode(',', $lines);
            for($i=0; $i<count($array_lines); $i++) {
            list($color, $offsetx1, $offsety1, $offsetx2, $offsety2) =
            explode(':', $array_lines[$i]);
            $color = $this->mMysqli->real_escape_string($color);
            $offsetx1 = $this->mMysqli->real_escape_string($offsetx1);
            $offsetx2 = $this->mMysqli->real_escape_string($offsetx2);
            $offsety1 = $this->mMysqli->real_escape_string($offsety1);
            $offsety2 = $this->mMysqli->real_escape_string($offsety2);
            $insert_line = 'INSERT INTO whiteboard ' . '
            (offsetx1, offsety1, offsetx2, offsety2, length, color, session_id) '
            .'VALUES (' . $offsetx1 . ',' . $offsety1 . ',' . $offsetx2 . ','
			. $offsety2 . ',' .
            sqrt(pow(($offsetx1-$offsetx2), 2) + pow(($offsety1-$offsety2), 2))
            . ',"'.$color. '","' . $session_id . '")';
             $this->mMysqli->query($insert_line);
            }
        }
    }
    public function getNewLines($id, $session_id) {
    $id = $this->mMysqli->real_escape_string($id);
    $session_id = $this->mMysqli->real_escape_string($session_id);
    $last_id = $this->getLastId();
    $get_lines =
    'SELECT whiteboard_id, color, offsetx1, offsety1, offsetx2, offsety2 '
    . 'FROM whiteboard ' .
    'WHERE whiteboard_id IN ' .
    ' (SELECT MAX(whiteboard_id) ' .
    ' FROM whiteboard ' .
    ' WHERE whiteboard_id> ' . $id .
    ' GROUP BY offsetx1, offsety1, offsetx2, offsety2) ' .
    'AND session_id<>"' . $session_id . '" ' .
    'ORDER BY whiteboard_id ASC';
    $result = $this->mMysqli->query($get_lines);
    $response = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>';
    $response .= '<response>';
    $response .= '<last_id>' . $last_id . '</last_id>';
    $response .= '<session_id>' . $session_id . '</session_id>';
    while($row = $result->fetch_array(MYSQLI_ASSOC)) {
    $id = $row['whiteboard_id'];
    $offsetx1 = $row['offsetx1'];
    $offsety1 = $row['offsety1'];
    $offsetx2 = $row['offsetx2'];
    $offsety2 = $row['offsety2'];
    $color = $row['color'];
    $response .= '<id>' . $id . '</id>' .
    '<color>' . $color . '</color>' .
    '<offsetx1>' . $offsetx1 . '</offsetx1>' .
    '<offsety1>' . $offsety1 . '</offsety1>' .
    '<offsetx2>' . $offsetx2 . '</offsetx2>' .
    '<offsety2>' . $offsety2 . '</offsety2>';
    }
    $result->close();
    $response .= '</response>';
    return $response;
    }
    public function clearWhiteboard() {
    $clear_wb = 'TRUNCATE TABLE whiteboard';
    $this->mMysqli->query($clear_wb);
    }
    private function getLastId() {
    $get_last_id = 'SELECT whiteboard_id ' .
    'FROM whiteboard ' .
    'ORDER BY whiteboard_id DESC ' .
    'LIMIT 1';
    $result = $this->mMysqli->query($get_last_id);
    if($result->num_rows > 0) {
    $row = $result->fetch_array(MYSQLI_ASSOC);
    return $row['whiteboard_id'];
    } else
    return '0';
    }
}
?>