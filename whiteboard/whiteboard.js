var whiteboardURL = "../whiteboard/whiteboard.php";
var mouseX = 0;
var mouseY = 0;
var isDrawing = false;
var isWhiteboardErased = false;
var lastDrawnLineId = 0;
var lastDbLineId = 0;
var sessionId = "";
var wbUpdatedLinesArray;
var linesCount = 0;
var oWhiteboard;
var oColor;
var wbOffsetLeft;
var wbOffsetTop;
var wbOffsetWidth;
var wbOffsetHeight;
var currentColor = "#000000";
var debugMode = true;
var oStatus;
window.onload = init;
var lineStartPointOffsetX=0;
var lineStartPointOffsetY=0;
var xmlHttpUpdateWhiteboard= createXmlHttpRequestObject();
var xmlHttpGetColor = createXmlHttpRequestObject();
function createXmlHttpRequestObject()
{
var xmlHttp;
try
{
xmlHttp = new XMLHttpRequest();
}
catch(e)
{
var XmlHttpVersions = new Array("MSXML2.XMLHTTP.6.0","MSXML2.XMLHTTP.5.0","MSXML2.XMLHTTP.4.0","MSXML2.XMLHTTP.3.0","MSXML2.XMLHTTP",
"Microsoft.XMLHTTP");
for (var i=0; i<XmlHttpVersions.length && !xmlHttp; i++)
{
	try
	{
	xmlHttp = new ActiveXObject(XmlHttpVersions[i]);
	}
	catch (e) {}
	}
}
if (!xmlHttp)
	alert("Error creating the XMLHttpRequest object.");
else
	return xmlHttp;
}
function drawline(x0, y0, x1, y1,color)
{
var dy = y1 - y0;
var dx = x1 - x0;
var stepx, stepy;

    if (dy < 0)
    {
	dy = -dy;
 	stepy = -1;
    }
    else
    {
	stepy = 1;
    }
    if (dx < 0)
    {
        dx = -dx;
        stepx = -1;
    } else {
        stepx = 1;
    }
    dx <<= 1;
    dy <<= 1;
    createPoint(x0, y0,color);
    if (dx > dy)
    {
        fraction = dy - (dx >> 1);
        while (x0 != x1)
        {
            if (fraction >= 0)
            {
                y0 += stepy;
                fraction -= dx;
            }

            x0 += stepx;

            fraction += dy;

            if(isInWhiteboard(x0,y0))
                createPoint(x0, y0,color);
            else
                return;
        }
    } else {
        fraction = dx - (dy >> 1);
        while (y0 !=y1)
{
if (fraction >= 0)
{
x0 += stepx;
fraction -= dy;
}
y0 += stepy;
fraction += dx;
if(isInWhiteboard(x0, y0))
createPoint(x0, y0,color);
else
return;
}
}
}
function isInWhiteboard(x,y)
{
    return (x < wbOffsetWidth-1 && x > 1 && y < wbOffsetHeight-1 && y > 1);
}
function handleMouseOut(e)
{
	getMouseXY(e);
	lineStopPointOffsetX = mouseX - wbOffsetLeft;
    lineStopPointOffsetY = mouseY - wbOffsetTop;
	if(isInWhiteboard(lineStopPointOffsetX, lineStopPointOffsetY))
return;
if(isDrawing == true)
{
	isDrawing = false;
	drawline(lineStartPointOffsetX, lineStartPointOffsetY,lineStopPointOffsetX, lineStopPointOffsetY,currentColor);
addLine(lineStartPointOffsetX, lineStartPointOffsetY,lineStopPointOffsetX, lineStopPointOffsetY,currentColor);
lineStartPointOffsetX = lineStopPointOffsetX;
lineStartPointOffsetY = lineStopPointOffsetY;
}
}
function handleMouseDown(e)
{
isDrawing = true;
if(!e) e = window.event;
getMouseXY(e);
lineStartPointOffsetX = mouseX - oWhiteboard.offsetLeft;
lineStartPointOffsetY = mouseY - oWhiteboard.offsetTop;
}
function handleMouseUp(e)
{
isDrawing = false;
if(!e) e = window.event;
getMouseXY(e);
lineStopPointOffsetX = mouseX - oWhiteboard.offsetLeft;
lineStopPointOffsetY = mouseY - oWhiteboard.offsetTop;

drawline(lineStartPointOffsetX, lineStartPointOffsetY,
lineStopPointOffsetX, lineStopPointOffsetY, currentColor);

addLine(lineStartPointOffsetX, lineStartPointOffsetY,lineStopPointOffsetX, lineStopPointOffsetY, currentColor);
}
function handleMouseMove(e)
{
if(isDrawing)
{
if(!e) e = window.event;
getMouseXY(e);
lineStopPointOffsetX = mouseX - oWhiteboard.offsetLeft;
lineStopPointOffsetY = mouseY - oWhiteboard.offsetTop;
drawline(lineStartPointOffsetX, lineStartPointOffsetY,lineStopPointOffsetX, lineStopPointOffsetY, currentColor);

addLine(lineStartPointOffsetX,lineStartPointOffsetY,lineStopPointOffsetX,lineStopPointOffsetY,currentColor);
lineStartPointOffsetX=lineStopPointOffsetX;
lineStartPointOffsetY=lineStopPointOffsetY;
}
}
function addLine(offsetX1, offsetY1, offsetX2, offsetY2, color)
{
var newLine = color.substring(1,7) + ":" + offsetX1 + ":" + offsetY1 + ":" + offsetX2 + ":" + offsetY2;
wbUpdatedLinesArray[linesCount++] = newLine;
}
function createPoint(offsetX, offsetY, color)
{
oDiv = document.createElement("div");
oDiv.className = "simple";
newColor = color;
oDivStyle = oDiv.style;
oDivStyle.backgroundColor = newColor;
oDivStyle.left = offsetX + "px";
oDivStyle.top = offsetY + "px";
oWhiteboard.appendChild(oDiv);
}
function init()
{
oWhiteboard = document.getElementById("whiteboard");
oColor = document.getElementById("color");
wbOffsetWidth = oWhiteboard.offsetWidth;
wbOffsetHeight = oWhiteboard.offsetHeight;
wbOffsetLeft = oWhiteboard.offsetLeft;
wbOffsetTop = oWhiteboard.offsetTop;
wbUpdatedLinesArray = new Array();
linesCount = 0;
oWhiteboard.setAttribute("onmousedown", "handleMouseDown(event);");
oWhiteboard.setAttribute("onmouseup", "handleMouseUp(event);");
if(oWhiteboard.onmousedown)
{
oWhiteboard.onmousedown=handleMouseDown;
oWhiteboard.onmouseup=handleMouseUp;
}
updateWhiteboard();
}
function clearWhiteboard()
{
lastDrawnLineId = 0;
linesCount = 0;
isWhiteboardErased = true;

while(oWhiteboard.hasChildNodes())
oWhiteboard.removeChild(oWhiteboard.lastChild);
}

function updateWhiteboard()
{
if(xmlHttpUpdateWhiteboard)
{
try
{
if (xmlHttpUpdateWhiteboard.readyState == 4 ||
xmlHttpUpdateWhiteboard.readyState == 0)
{
params = "";
if(isWhiteboardErased == true)
{
params += "session_id=" + sessionId + "&mode=DeleteAndRetrieve";
isWhiteboardErased = false;
}
else
{
if(linesCount > 0)
{
params="session_id=" + sessionId + "&mode=SendAndRetrieve" + "&last_id=" + lastDrawnLineId + "&lines=";

for(i=0; i<linesCount; i++)
{
params += wbUpdatedLinesArray[i];
params += (i < linesCount-1) ? "," : "";
}
linesCount=0;
}
else
{
params="session_id=" + sessionId + "&mode=Retrieve&" + "last_id=" + lastDrawnLineId;
}
}
xmlHttpUpdateWhiteboard.open("POST", whiteboardURL, true);
xmlHttpUpdateWhiteboard.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
xmlHttpUpdateWhiteboard.onreadystatechange = handleUpdatingWhiteboard;
xmlHttpUpdateWhiteboard.send(params);
}
else
{
setTimeout("updateWhiteboard();", 1000);
}
}
catch(e){}
}
else
{
alert("The XMLHttpRequest object is null !");
}
}
function handleUpdatingWhiteboard()
{
if (xmlHttpUpdateWhiteboard.readyState == 4)
{
if (xmlHttpUpdateWhiteboard.status == 200)
{
try
{
displayUpdates();
}
catch(e){}
}
else
{
alert("There was a problem when updating the whiteboard :\n" + xmlHttpUpdateWhiteboard.statusText);
}
}
}
function displayUpdates()
{
response = xmlHttpUpdateWhiteboard.responseText;

if (response.indexOf("ERRNO") >= 0
|| response.indexOf("error:") >= 0
|| response.length == 0)
throw(response.length == 0 ? "Can't update the whiteboard!" :response);

response = xmlHttpUpdateWhiteboard.responseXML.documentElement;
sessionId =response.getElementsByTagName("session_id").item(0).firstChild.data;
newLastDbLineId =parseInt(response.getElementsByTagName("last_id").item(0).firstChild.data);

if(newLastDbLineId < lastDbLineId)
{
clearWhiteboard(oWhiteboard);
isWhiteboardErased = false;
}
else
if(newLastDbLineId>lastDbLineId)
{
idArray= response.getElementsByTagName("id");
colorArray= response.getElementsByTagName("color");
offsetX1Array= response.getElementsByTagName("offsetx1");
offsetY1Array=response.getElementsByTagName("offsety1");
offsetX2Array= response.getElementsByTagName("offsetx2");
offsetY2Array=response.getElementsByTagName("offsety2");

if(idArray.length>0)
updateLines(idArray,colorArray,offsetX1Array,offsetY1Array,offsetX2Array,offsetY2Array);
}
lastDbLineId = newLastDbLineId;
setTimeout("updateWhiteboard();", 1000);
}
function updateLines(idArray, colorArray, offsetX1Array, offsetY1Array,offsetX2Array, offsetY2Array)
{
for(var i=0; i<idArray.length; i++)
{
drawline(parseInt(offsetX1Array[i].firstChild.data),parseInt(offsetY1Array[i].firstChild.data),parseInt(offsetX2Array[i].firstChild.data),
parseInt(offsetY2Array[i].firstChild.data),"#"+colorArray[i].firstChild.data);
}
lastDrawnLineId=idArray[i-1].firstChild.data;
}
function handleGettingColor()
{
if (xmlHttpGetColor.readyState == 4)
{
if (xmlHttpGetColor.status == 200)
{
try
{
changeColor();
}
catch(e)
{
alert(e.toString() + "\n" + xmlHttpGetColor.responseText);
}
}
else
{
alert("There was a problem retrieving the color:\n" +
xmlHttpGetColor.statusText);
}
}
}
function getMouseXY(e)
{
 	if(document.all)
	{
   	mouseX = window.event.x + document.body.scrollLeft;
    	mouseY = window.event.y + document.body.scrollTop;
	}
	else
	{
	mouseX = e.pageX;
	mouseY = e.pageY;
 }
}