<?php
require_once('../whiteboard/whiteboard.class.php');
$wb = new Whiteboard();
$wb->checkLoad();
$mode = $_POST['mode'];
$session_id = $_POST['session_id'];
if($session_id == '')
$session_id = md5(uniqid());
$last_id = 0;
if($mode == 'DeleteAndRetrieve')
$wb->clearWhiteboard();
elseif($mode == 'SendAndRetrieve')
{
$lines = $_POST['lines'];
$last_id = $_POST['last_id'];
$wb->insertLines($lines, $session_id);
}
elseif($mode == 'Retrieve')
{
$last_id = $_POST['last_id'];
}
if(ob_get_length()) ob_clean();

header('Expires: Fri, 25 Dec 1980 00:00:00 GMT'); // time in the past
header('Last-Modified: ' . gmdate( 'D, d M Y H:i:s') . 'GMT');
header('Cache-Control: no-cache, must-revalidate');
header('Pragma: no-cache');
header('Content-Type: text/xml');
echo $wb->getNewLines($last_id, $session_id);
?>